import { BadRequestException, PipeTransform } from '@nestjs/common'
import { TaskStatus } from '../task-status.enum'

export class TaskStatusValidationPipe implements PipeTransform {
  transform(value: any): any {
    value = value.toUpperCase()

    if (!this.isStatusValid(value)) {
      throw new BadRequestException('Invalid status')
    }

    return value
  }

  private isStatusValid(status: any) {
    return Object.values(TaskStatus).indexOf(status) !== -1
  }
}
