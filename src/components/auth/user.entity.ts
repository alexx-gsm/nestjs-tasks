import { BaseEntity, Entity, OneToMany } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { PrimaryGeneratedColumn, Column, Unique } from 'typeorm';
import { UserRole } from './user-role.enum';
import { Task } from '../tasks/task.entity';

@Entity()
@Unique(['email'])
export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  email: string;

  @Column()
  salt: string;

  @Column()
  hash: string;

  @Column()
  role: UserRole;

  @OneToMany(
    (type) => Task,
    (task) => task.user,
    {eager: true}
  )
  tasks: Task[];

  async validatePassword(password: string): Promise<boolean> {
    return await bcrypt.compare(password, this.hash);
  }
}
