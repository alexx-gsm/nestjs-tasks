import { UserRole } from './user-role.enum';

export interface JwtPayloadInterface {
  email: string;
  role: UserRole;
}
