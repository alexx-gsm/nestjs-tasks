export enum UserRole {
  ROLE_ADMIN = 'ADMIN',
  ROLE_OPERATOR = 'OPERATOR',
  ROLE_COURIER = 'COURIER',
  ROLE_USER = 'USER',
}
