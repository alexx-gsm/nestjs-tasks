import { IsEmail, IsString, Matches, MaxLength, MinLength } from 'class-validator'

export class AuthCredentialsDto {
  @IsEmail()
  email: string

  @IsString()
  @MinLength(6)
  @MaxLength(20)
  @Matches(/((?=.*\d)|(?![.\n]))(?=.*[A-Z])(?=.*[a-z]).*$/, {message: 'password too week'})
  password: string
}