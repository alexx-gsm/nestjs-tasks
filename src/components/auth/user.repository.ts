import { EntityRepository, Repository } from 'typeorm';
import { User } from './user.entity';
import { AuthCredentialsDto } from './dto/auth-credentials.dto';
import { UserRole } from './user-role.enum';
import { ConflictException, InternalServerErrorException } from '@nestjs/common';
import { DB_ERROR_DUBLICATE_FIELD } from '../../config/typeorm.constants';
import * as bcrypt from 'bcrypt';

@EntityRepository(User)
export class UserRepository extends Repository<User> {
  async signUp(authCredentialsDto: AuthCredentialsDto): Promise<void> {
    const { email, password } = authCredentialsDto;

    const salt = await bcrypt.genSalt();

    const user = new User();
    Object.assign(user, {
      email,
      salt,
      hash: await this.hashPassword(password, salt),
      role: UserRole.ROLE_USER,
    });

    try {
      await user.save();
    } catch (error) {
      if (error.code === DB_ERROR_DUBLICATE_FIELD) {
        throw new ConflictException('Email already exist');
      } else {
        throw new InternalServerErrorException('Internal server error');
      }
    }
  }

  async validateUser(authCredentialsDto: AuthCredentialsDto) {
    const { email, password } = authCredentialsDto;
    const user = await this.findOne({ email });

    if (user && (await user.validatePassword(password))) {
      return user
    } else {
      return null
    }
  }

  private async hashPassword(password: string, salt: string) {
    return bcrypt.hash(password, salt);
  }
}
